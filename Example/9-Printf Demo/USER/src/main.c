/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2020,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：一群：179029047(已满)  二群：244861897(已满)  三群：824575535
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file       		main
 * @company	   		成都逐飞科技有限公司
 * @author     		逐飞科技(QQ790875685)
 * @version    		查看doc内version文件 版本说明
 * @Software 		MDK5.27
 * @Target core		STC8A8K64S4
 * @Taobao   		https://seekfree.taobao.com/
 * @date       		2020-4-14
 ********************************************************************************************************************/

#include "headfile.h"



#define DIR P35
int16 dat = 0;


void main()
{
	DisableGlobalIRQ();	//关闭总中断
	board_init();	
	//uart_init(DEBUG_UART,DEBUG_UART_RX_PIN,DEBUG_UART_TX_PIN,DEBUG_UART_BAUD);
	//UART1已在board_init 中初始化，波特率115200

	EnableGlobalIRQ();	//开启总中断
    while(1)
	{
		printf("SEEKFREE Printf Test\r\n");
		printf("BAUD = 115200 \r\n");
    }
}











